package ru.badshurin.dao;

import org.hibernate.service.spi.ServiceException;

import java.util.ArrayList;

public interface PostDao {
    PostsEntity byName(String name) throws ServiceException;
    void addPost(PostsEntity postsEntity);
    ArrayList<PostsEntity> findAllPost();
    PostsEntity findById(int id);
    void deletePost(PostsEntity user);
}
