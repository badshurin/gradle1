package ru.badshurin.ws;

import ru.badshurin.dao.PostDao;
import ru.badshurin.dao.PostDaoImpl;
import ru.badshurin.dao.PostsEntity;

import javax.jws.WebService;
import java.util.ArrayList;

@WebService(endpointInterface = "ru.badshurin.ws.HelloWebService")
public class HelloWebServiceImpl implements HelloWebService {

    private PostDao postDao = new PostDaoImpl();

    @Override
    public String getHelloString(String name) {
        return "Hello, " + name + "!";
    }

    @Override
    public PostsEntity addPost(PostsEntity user){
                postDao.addPost(user);
        return user;
    }

    @Override
    public ArrayList<PostsEntity> findAllPost1() {
        return postDao.findAllPost();
    }

    @Override
    public PostsEntity getPostId(int postNo) {
        return postDao.findById(postNo);
    }

    @Override
    public PostsEntity getPostName(String postName) {
        return postDao.byName(postName);
    }

    @Override
    public void deletePostWS(PostsEntity user) {
        postDao.deletePost(user);
    }

}
